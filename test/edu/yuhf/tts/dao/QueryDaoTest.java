package edu.yuhf.tts.dao;

import java.util.List;

import org.junit.Test;

import edu.yuhf.tts.domain.Train;

public class QueryDaoTest {

	private QueryDao queryDao=new QueryDao();
	
	@Test
	public void queryTrainTest() {
		Train train=new Train(0,"","上海虹桥","北京南",null,null,null);
		List<Train> list=queryDao.queryTrain(train);
		list.forEach((train0)->{
			System.out.println(train0.getTrainId()+"车次，"+train0.getToAddress()+"到"+train0.getFromAddress()+",发车时间是："+train0.getDate());
		});
	}
}
