package edu.yuhf.jdbcpart;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.Test;

import edu.yuhf.domain.User;

public class UserTableOperationTest {

	private static UserTableOperation uto=new UserTableOperation();
	
	@Test
	public void addUserTest() {
		User user0=new User(0,"ruby","ruby","0",LocalDateTime.now());
		int result=uto.addUser(user0);
		System.out.println("row number:"+result);
	}
	@Test
	public void queryByIdTest() {
		User user=uto.queryById(2);
		System.out.println(user.getName()+","+user.getPassword());
	}

	@Test
	public void queryAllTest() {
		List<User> list=uto.queryAll();
		list.forEach((item)->System.out.println(item.getName()+","+item.getPassword()));
	}

}
