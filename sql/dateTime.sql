create table dateTable(
       id number(10) primary key,
       dateColumn date,				
       timeColumn date,				
       timestampColumn timestamp	
);
create sequence dateTable_id start with 0 minvalue 0 increment by 1;

insert into dateTable 
       values(dateTable_id.nextval,to_date(now(),"yyyy-mm-dd"),
       to_date(now(),"hh24:mi:ss"),to_timestamp(now(),"yyyy-mm-dd hh24:mi:ss.ff"));
