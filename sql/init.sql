--create table space
create tablespace testDB
datafile 'D:\app\Administrator\oradata\orcl\testdb.dbf'
size 100m;
--create coustomer
create user testdb identified by testdb default tablespace testDB;

grant connect to testdb;
grant resource to testdb;

connect testdb/testdb;           --sqlplus command

--create user table
create table users(
       id number(10) primary key,
       name varchar2(50) not null,
       password varchar2(50) not null,
       sex char(1) default '1',   --'1' man��'0' woman
       reg_date date
);
create sequence users_id minvalue 0 increment by 1 start with 1;

insert into users(id,name,password,sex) values(users_id.nextval,'admin','666','1');
insert into users(id,name,password,sex) values(users_id.nextval,'test','test','0');
commit;

select * from users;
