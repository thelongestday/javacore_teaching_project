create table tts_userInfo(
       id number(10) primary key,
       userName varchar2(50) not null,
       password varchar2(50) not null,
       sex char(1) default '1',   --1 is man,2 is woman
       idNumber varchar2(18),
       phone varchar2(20)
);

create table tts_trainInfo(
       id number(10) primary key,
       trainId varchar2(50) unique,
       fromAddress varchar2(50) not null,
       toAddress varchar2(50) not null,
       startDate date,
       startTime date,
       fares number(10,2) 
);
create sequence trainInfo_id minvalue 0 start with 1 increment by 1;
create table tts_orderform(
       id number(10) primary key,
       userId number(10) references tts_userInfo(id),
       trainId varchar2(50) references tts_trainInfo(trainId),
       byDate date,
       ticketNumber number(5),
       totalFares number(5,2)
);

insert into tts_trainInfo values(trainInfo_id.nextval
,'G124','������','�Ϻ�����',to_date('2018-08-23','yyyy-mm-dd'),to_date('11:00:00','hh24:mi:ss'),400);
commit;
select * from tts_trainInfo 
where toaddress='������' and fromAddress='�Ϻ�����' and startdate=to_date('2018-08-23','yyyy-mm-dd')
and trainId='';


--登录
select count(*) from tts_userInfo where userName=? and password=?;
