package edu.yuhf.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import edu.yuhf.dbutil.ResultSetHandler;

public class User2List implements ResultSetHandler<List<User>>{

	@Override
	public List<User> handler(ResultSet rs) {
		List<User> list0=new ArrayList<>();
		try {
			while(rs.next()) {
				User user=new User(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),LocalDateTime.now());
				list0.add(user);				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list0;
	}

}
