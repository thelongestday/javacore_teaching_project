package edu.yuhf.domain;

import java.time.LocalDateTime;

public class User {

	private int id;
	private String name;
	private String password;
	private String sex;
	private LocalDateTime date;
	
	public User() {}

	public User(int id, String name, String password, String sex, LocalDateTime date) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.sex = sex;
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}
}
