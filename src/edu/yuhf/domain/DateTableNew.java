package edu.yuhf.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class DateTableNew {

	private int id;
	private LocalDate datetime;
	private LocalTime time;
	private LocalDateTime ts;
	
	public DateTableNew() {}

	public DateTableNew(int id, LocalDate datetime, LocalTime time, LocalDateTime ts) {
		super();
		this.id = id;
		this.datetime = datetime;
		this.time = time;
		this.ts = ts;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getDatetime() {
		return datetime;
	}

	public void setDatetime(LocalDate datetime) {
		this.datetime = datetime;
	}

	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}

	public LocalDateTime getTs() {
		return ts;
	}

	public void setTs(LocalDateTime ts) {
		this.ts = ts;
	}
}
