package edu.yuhf.domain;

import java.sql.Timestamp;
import java.util.Date;

public class DateTable {

	private int id;
	private Date date;
	private Date dateTime;
	private Timestamp ts;
	public DateTable(int id, Date date, Date dateTime, Timestamp ts) {
		super();
		this.id = id;
		this.date = date;
		this.dateTime = dateTime;
		this.ts = ts;
	}
	
	public DateTable() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public Timestamp getTs() {
		return ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}
	
}
