package edu.yuhf.pac;

import java.util.LinkedList;

public interface Storage {
	public final int MAX_NUM=100;
	LinkedList<Product> list=new LinkedList<>();
	
	public void produce(int number);
	public void buy(int number);
}
