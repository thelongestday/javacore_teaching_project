package edu.yuhf.pac;

public class StorageForSynchronized implements Storage{

	public void produce(int number) {
		synchronized(list) {
			while(list.size()+number>=MAX_NUM) {
				System.out.println("仓库已满，无法生产！"+number+"件产品！");
				try {
					list.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			for(int i=0;i<number;i++) {
				list.add(new Product(i,Thread.currentThread().getName()+"_"+i));
			}
			System.out.println("已经生产了"+number+"件产品,仓库中共有"+list.size()+"件产品");
			list.notifyAll();
		}
	}
	public void buy(int number) {
		synchronized(list) {
			while(list.size()-number<=0) {
				System.out.println("仓库存货不足，无法消费"+number+"件产品！");
				try {
					list.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			for(int i=0;i<number;i++) {
				list.remove();
			}
			System.out.println("已经消费了"+number+"件产品，仓库中还有"+list.size()+"件产品");
			list.notifyAll();
		}
	}
}
