package edu.yuhf.pac;

public class Producer extends Thread{
	
	private int num;
	private Storage storage;
	
	public Producer(int num,Storage storage) {
		this.num=num;
		this.storage=storage;
	}
	
	public void run() {
		storage.produce(num);
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

}
