package edu.yuhf.pac;

public class Test {

	public static void main(String[] args) {
		
		Storage storage=new StorageForSynchronized();
		//Storage storage=new StorageForReentrantLock();
		
		Producer p1=new Producer(30,storage);
		Producer p2=new Producer(20,storage);
		Producer p3=new Producer(50,storage);
		
		Customer c1=new Customer(20,storage);
		Customer c2=new Customer(8,storage);
		Customer c3=new Customer(4,storage);
		
		Customer c4=new Customer(10,storage);
		Customer c5=new Customer(11,storage);
		Customer c6=new Customer(30,storage);
		
		p1.start();
		p2.start();
		p3.start();
		
		c1.start();
		c2.start();
		c3.start();
		c4.start();
		c5.start();
		
		c6.start();
	}
}
