package edu.yuhf.threaddemo;

public class ThreadTest {
	public static void main(String[] args) {
		/*
		ThreadDemo td=new ThreadDemo();
		td.start();
		*/
		
		/*
		RunnableDemo rd=new RunnableDemo();
		Thread thread=new Thread(rd);
		thread.start();
		*/
		
		new Thread(()-> {
			for(int i=0;i<=10000;i++) {
				System.out.print(i+",");
			}
			System.out.println(Thread.currentThread().getName());
		}).start();
		
		for(int i=10000;i>0;i--) {
			System.out.print(i+",");
		}
		System.out.println(Thread.currentThread().getName());
	}
}
