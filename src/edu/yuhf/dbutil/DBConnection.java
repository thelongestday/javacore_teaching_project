package edu.yuhf.dbutil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
	
	private final static String URL;
	private final static String USERNAME;
	private final static String PASSWORD;
	private final static String CLASSNAME;
	static {
		Properties prop=new Properties();
		try {
			prop.load(DBConnection.class.getResourceAsStream("/jdbc.properties"));
		} catch (IOException e) {
			System.out.println("jdbc.properties file not found,error message is "+e.getMessage());
		}
		URL=prop.getProperty("jdbc.url");
		USERNAME=prop.getProperty("jdbc.userName");
		PASSWORD=prop.getProperty("jdbc.password");
		CLASSNAME=prop.getProperty("jdbc.className");
		try {
			Class.forName(CLASSNAME);
		} catch (ClassNotFoundException e) {
			System.out.println("Class not found,error message is "+e.getMessage());
		}
	}
	
	public static Connection getConnection() {
		Connection connection=null;
		try {
			connection=DriverManager.getConnection(URL, USERNAME, PASSWORD);
		} catch (SQLException e) {
			System.out.println("database connection error,message is "+e.getMessage());
		}
		return connection;
	}
	
	public static void closeConnection(Connection connection) {
		if(null!=connection) {
			try {
				connection.close();
			} catch (SQLException e) {
				System.out.println("database close error,message is "+e.getMessage());
			}
		}
	}
}
