package edu.yuhf.dbutil;

import java.sql.ResultSet;

@FunctionalInterface
public interface ResultSetHandler<T> {
	public T handler(ResultSet rs);
}
