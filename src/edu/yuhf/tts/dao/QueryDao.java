package edu.yuhf.tts.dao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import edu.yuhf.dbutil.JdbcTemplate;
import edu.yuhf.tts.domain.Train;
import edu.yuhf.tts.domain.Train2List;

public class QueryDao {

	public BigDecimal queryByTrainId(String trainId,LocalDate date) {
		String sql="select fares from tts_trainInfo where to_date(?,'yyyy-mm-dd') and trainId=?";
		
		return null;
	}
	
	public int checkUser(String userName,String password) {
		int rowNum=0;
		String sql="select count(*) from tts_userInfo where userName=? and password=?";
		
		return rowNum;
	}
	
	public List<Train> queryTrain(Train train){
		//拼装SQL
		String sql="select * from tts_trainInfo " + 
				"where fromAddress=? and toaddress=?";
		if(train.getTrainId().length()!=0) {
			sql+=" and trainId=?";
		}
		if(null!=train.getDate()) {
			sql+=" and startdate=to_date(?,'yyyy-mm-dd')";
		}

		//组装参数
		List<Object> params=new ArrayList<>();
		params.add(train.getFromAddress());
		params.add(train.getToAddress());
		
		if(train.getTrainId().length()!=0) {
			params.add(train.getTrainId());
		}
		if(null!=train.getDate()) {
			DateTimeFormatter dft=DateTimeFormatter.ofPattern("yyyy/MM/dd");
			params.add(dft.format(train.getDate()));
		}		
		
		List<Train> list=JdbcTemplate.query(sql,new Train2List(), params.toArray());
		return list;
	}
}
