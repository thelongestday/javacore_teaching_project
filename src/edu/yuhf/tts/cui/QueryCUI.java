package edu.yuhf.tts.cui;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import edu.yuhf.tts.domain.Train;
import edu.yuhf.tts.service.QueryService;

public class QueryCUI {

	private QueryService queryService=new QueryService();
	
	public void ShowQuery() {
		System.out.println("车次查询，请按照要求输入查询关键字：");
		Scanner input=new Scanner(System.in);
		System.out.print("出发地：");
		String fromAddress=input.nextLine();
		System.out.print("目的地：");
		String toAddress=input.nextLine();
		System.out.print("发车日期：(2018-08-23)");
		String date=input.nextLine();
		System.out.print("车次");
		String trainId=input.nextLine();
		//jdk1.8之后的日期处理方式
		Train train=null;
		if(!Objects.equals(date, "")) {
			DateTimeFormatter dft=DateTimeFormatter.ofPattern("yyyy-MM-dd");
			train=new Train(0,trainId,fromAddress,toAddress,LocalDate.parse(date,dft),null,null);
		}else {
			train=new Train(0,trainId,fromAddress,toAddress,null,null,null);
		}
		
		List<Train> list=queryService.queryTrain(train);
		System.out.println("查询结果:");
		if(list.size()==0) {
			System.out.println("没有查询到相关数据！");
		}
		list.forEach((train0)->{
			System.out.println(train0.getTrainId()+"车次，"+train0.getFromAddress()+"到"+train0.getToAddress()+",发车时间是："+train0.getDate());
		});
		input.close();
	}
	

}
