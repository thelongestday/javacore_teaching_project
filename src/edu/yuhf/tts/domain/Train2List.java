package edu.yuhf.tts.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.yuhf.dbutil.ResultSetHandler;

public class Train2List implements ResultSetHandler<List<Train>>{

	public List<Train> handler(ResultSet rs) {
		List<Train> list0=new ArrayList<>();
		try {
			while(rs.next()) {
				Train train0=new Train(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getDate(5).toLocalDate(),rs.getTime(6).toLocalTime(),rs.getBigDecimal(7));
				list0.add(train0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list0;
	}

}
