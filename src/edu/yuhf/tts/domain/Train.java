package edu.yuhf.tts.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

public class Train {

	private int id;
	private String trainId;
	private String fromAddress;
	private String toAddress;
	private LocalDate  date;
	private LocalTime  time;
	private BigDecimal fares;
	
	public Train() {}

	public Train(int id, String trainId, String fromAddress,String toAddress, LocalDate date, LocalTime time,
			BigDecimal fares) {
		super();
		this.id = id;
		this.trainId = trainId;
		this.toAddress = toAddress;
		this.fromAddress = fromAddress;
		this.date = date;
		this.time = time;
		this.fares = fares;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTrainId() {
		return trainId;
	}

	public void setTrainId(String trainId) {
		this.trainId = trainId;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}

	public BigDecimal getFares() {
		return fares;
	}

	public void setFares(BigDecimal fares) {
		this.fares = fares;
	}
	
}
