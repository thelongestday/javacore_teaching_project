package edu.yuhf.tts.service;

import java.util.List;

import edu.yuhf.tts.dao.QueryDao;
import edu.yuhf.tts.domain.Train;
import edu.yuhf.tts.domain.User;

public class QueryService {

	private QueryDao queryDao=new QueryDao();
	
	public List<Train> queryTrain(Train train) {
		return queryDao.queryTrain(train);
	}
	
	public boolean loginUser(User user) {
		boolean flag=false;
		if(queryDao.checkUser(user.getUserName(), user.getPassword())==1) {
			flag=true;
		}
		return flag;
	}
}
