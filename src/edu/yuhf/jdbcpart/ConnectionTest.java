package edu.yuhf.jdbcpart;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConnectionTest {

	public static void main(String[] args) {
		String url="jdbc:oracle:thin:@localhost:1521:orcl";
		String sql="select * from users";
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.out.println("Class not found,error message is "+e.getMessage());
		}
		
		try(Connection connection=DriverManager.getConnection(url, "testdb","testdb")){
			PreparedStatement psmt=connection.prepareStatement(sql);
			ResultSet rs=psmt.executeQuery();
			//ORM
			while(rs.next()) {
				System.out.print(rs.getInt(1)+",");
				System.out.print(rs.getString(2)+",");
				System.out.print(rs.getString(3)+",");
				System.out.print(rs.getString(4));
				System.out.println();
			}
		} catch (SQLException e) {
			System.out.println("SQL error message is "+e.getMessage());
			e.printStackTrace();
		}
	}
}
