package edu.yuhf.jdbcpart;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import edu.yuhf.dbutil.JdbcTemplate;
import edu.yuhf.domain.DateTable;

public class DateTimeJdbcOperation {

	@Test
	public void insertDateTime() {
		String sql="insert into dateTable " + 
				"values(dateTable_id.nextval,to_date(?,'yyyy-mm-dd')," + 
				"to_date(?,'yyyy-mm-dd hh24:mi:ss'),to_timestamp(?,'yyyy-mm-dd hh24:mi:ss.ff'))";
		Date date=new Date();
		SimpleDateFormat sdf_date=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf_time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf_ts=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		int rowNum=JdbcTemplate.update(sql, sdf_date.format(date),sdf_time.format(date),sdf_ts.format(date));
		System.out.println("insert dateTime row Number:" +rowNum);
	}
	
	@Test
	public void showDataTime() {
		String sql="select * from dateTable";
		List<DateTable> list=JdbcTemplate.query(sql, (rs)->{
			List<DateTable> list0=new ArrayList<>();
			try {
				while(rs.next()) {
					DateTable dt=new DateTable(rs.getInt(1),rs.getDate(2),rs.getTime(3),rs.getTimestamp(4));
					list0.add(dt);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return list0;
		}, new Object[] {});
		
		list.forEach((item)->{
			System.out.println(item.getDate()+","+item.getDateTime());
		}) ;
	}
}
