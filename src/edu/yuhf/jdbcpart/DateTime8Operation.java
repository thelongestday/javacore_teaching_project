package edu.yuhf.jdbcpart;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import edu.yuhf.dbutil.JdbcTemplate;
import edu.yuhf.domain.DateTableNew;

public class DateTime8Operation {
	@Test
	public void insertDateTime() {
		String sql="insert into dateTable " + 
				"values(dateTable_id.nextval,to_date(?,'yyyy-mm-dd')," + 
				"to_date(?,'yyyy-mm-dd hh24:mi:ss'),to_timestamp(?,'yyyy-mm-dd hh24:mi:ss.ff'))";
		LocalDate date=LocalDate.now();
		DateTimeFormatter dtf=DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime dt=LocalDateTime.now();
		DateTimeFormatter dtf1=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dt1=LocalDateTime.now();
		DateTimeFormatter dtf2=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		int rowNum=JdbcTemplate.update(sql, dtf.format(date),dtf1.format(dt),dtf2.format(dt1));
		System.out.println("insert dateTime row Number:" +rowNum);
	}
	
	@Test
	public void showDataTime() {
		String sql="select * from dateTable";
		List<DateTableNew> list=JdbcTemplate.query(sql, (rs)->{
			List<DateTableNew> list0=new ArrayList<>();
			try {
				while(rs.next()) {
					DateTableNew dt=new DateTableNew(rs.getInt(1),rs.getDate(2).toLocalDate(),rs.getTime(3).toLocalTime(),rs.getTimestamp(4).toLocalDateTime());
					list0.add(dt);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return list0;
		}, new Object[] {});
		
		list.forEach((item)->{
			System.out.println(item.getDatetime()+","+item.getTime());
		}) ;
	}
}
