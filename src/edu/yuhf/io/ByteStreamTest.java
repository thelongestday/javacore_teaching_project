package edu.yuhf.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ByteStreamTest {

	public static void main(String[] args) {
		try (InputStream is=new FileInputStream("e:\\example.txt");
				OutputStream os=new FileOutputStream("e:\\yuhf\\1.txt");){
			byte[] buffer=new byte[128];
			while(is.read(buffer)!=-1){
				os.write(buffer);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
