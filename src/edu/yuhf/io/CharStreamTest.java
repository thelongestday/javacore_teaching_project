package edu.yuhf.io;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CharStreamTest {

	public static void main(String[] args) {
		try (FileReader fr=new FileReader("e:\\example.txt");
				FileWriter fw=new FileWriter("e:\\yuhf\\1.txt");){
			char[] buffer=new char[1024];
			while(fr.read(buffer)!=-1) {
				fw.write(buffer);
				fw.flush();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
